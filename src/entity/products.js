export class Products{
    id;
    category;
    label;
    price;
    description;

    constructor(cat, label, price, desc, id){
        this.id = id;
        this.category = cat;
        this.label = label;
        this.price = price;
        this.description = desc;
    }
}