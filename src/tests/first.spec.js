import request from "supertest";
import { connection } from "../repository/connection";
import { ProductRepo } from "../repository/product_repository";
import {
    server
} from "../server";

describe('First api tests', () => {


    it('should send success on /api/products/show/:id', async () => {
        let response = await request(server)
            .get('/api/products/show/2')
            .expect(200)
            .expect('Content-Type', /json/)

        expect(response.body).toEqual({
            category: expect.any(String),
            description: expect.any(String),
            id: expect.any(Number),
            label: expect.any(String),
            price: expect.any(Number)

        });

    });

    it('should send success on /show/cat/:name with a limit 1 and an offset 0', async () => {
        let categoryTry = ['fruits', 'toys', 'cycling'];
        let index = Math.floor(Math.random(2));
        let response = await request(server)
            .get(`/api/products/show/cat/${categoryTry[index]}?limit=1&offset=0`)
            .expect(200)
            .expect('Content-Type', /json/)

        expect(response.body).toEqual([{
            category: categoryTry[index],
            description: expect.any(String),
            id: expect.any(Number),
            label: expect.any(String),
            price: expect.any(Number)
        }])
    })

    it('should add a product and send success', async () => {
        await request(server)
            .post('/api/products/add')
            .send({
                label: 'test label',
                category: 'categoryTest2',
                price: 2.54,
                description: 'test description'
            })
            .expect(200)

        expect(await new ProductRepo().findByCategory("categoryTest2", 1, 0)).toEqual([{
            label: expect.any(String),
            category: 'categoryTest2',
            id: expect.any(Number),
            price: expect.any(Number),
            description: expect.any(String)
        }])
        await connection.execute('DELETE FROM Product WHERE category="categoryTest2" ORDER BY id desc LIMIT 1')
    })

    it('should update an item with an id', async ()=>{
        await request(server)
        .put('/api/products/update')
        .send({
            "label":"kiki",
            "category": "cat",
            "price": 2500.45,
            "description":"a nice cat",
            "id":1
        })
        .expect(200)

        expect(await new ProductRepo().findById(1)).toEqual({
            "label":"kiki",
            "category": "cat",
            "price": 2500.45,
            "description":"a nice cat",
            "id":1
        })
    })

    it('should display the searched product and send success', async ()=>{
        let param1 = "kiki"
        let response = await request(server)
        .get(`/api/products/search?find=${param1}`)
        .expect(200)

        expect(response.body[0]).toEqual({
            label: expect.any(String),
            category: expect.any(String),
            id: expect.any(Number),
            price: expect.any(Number),
            description: expect.any(String)
        })
    })
});