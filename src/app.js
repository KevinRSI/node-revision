import "dotenv-flow/config.js";
import { ProductRepo } from "./repository/product_repository.js";
import { server } from "./server.js";



let port = process.env.PORT || 3000;

server.listen(port, ()=>{
    console.log('Server is running on port '+ port);
})
