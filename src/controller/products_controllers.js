import {
    Router
} from "express";
import {
    ProductRepo
} from "../repository/product_repository.js";

export const productsRoute = Router();

productsRoute.get('/show/:id', async (req, res) => {
    let data = await new ProductRepo().findById(req.params.id);
    res.json(data);
    res.end();
})

productsRoute.get('/show/cat/:name', async (req, res) => {
    let data = await new ProductRepo().findByCategory(req.params.name, req.query.limit, req.query.offset);
    res.json(data);
    res.end();
})


productsRoute.post('/add', async (req, res) => {
    await new ProductRepo().addProducts(req.body);
    res.end();
})

productsRoute.put('/update', async (req, res)=>{
    await new ProductRepo().updateProduct(req.body);
    res.end();
})

productsRoute.get('/search', async (req, res)=>{
    let data = await new ProductRepo().searchProduct(`%${req.query.find}%`);
    res.json(data);
    res.end();
})