
import express from "express";
import { productsRoute } from "./controller/products_controllers.js";

export const server = express();

server.use(express.json());
server.use(express.urlencoded({extended: true}));

//conrollers
server.use('/api/products', productsRoute);





