import { Products } from "../entity/products.js";
import { connection } from "./connection.js";

export class ProductRepo{
    /**
     * 
     * @param {string} cat 
     * @returns 
     */
    async findByCategory(cat, limit, offset){
        let [rows] = await connection.execute('SELECT * FROM Product WHERE category=? LIMIT ? OFFSET ?' , [cat, limit, offset]);
        let products = [];
        for (const row of rows) {
            let instance = new Products(row.category, row.label, row.price, row.description, row.id);
            products.push(instance);
        }
        return products;
    }
    
    async findById(id){
        let [rows] = await connection.execute('SELECT * FROM Product WHERE id=?', [id]);
        let instance = new Products(rows[0].category, rows[0].label, rows[0].price, rows[0].description, rows[0].id);
        return instance;
    }

    async addProducts(newProd){
        await connection.execute('INSERT INTO Product (label, category, price, description) VALUES(?, ?, ?, ?)', [newProd.label, newProd.category, newProd.price, newProd.description]);
    }

    async updateProduct(updateProd){
        await connection.execute('UPDATE Product SET label=?, category=?, price=?, description=? WHERE id=?',[updateProd.label, updateProd.category, updateProd.price, updateProd.description, updateProd.id])
    }

    async searchProduct(search){
        let [rows] = await connection.execute('SELECT * FROM Product WHERE label LIKE ? OR description LIKE ?', [search, search]);
        let items = [];
        for (const row of rows) {
            let instance = new Products(row.category, row.label, row.price, row.description, row.id);
            items.push(instance)
        }
        return items;
    }
}