
DROP TABLE IF EXISTS Product;
CREATE TABLE `Product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `category` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO Product(id,category,label,price,description) VALUES(1,'fruits','banana',1.24,'just some bananas'),(2,'food','magnum white',2.65,'vanilla ice cream with white chocolate');